# Allgemeine Infos zur (mündlichen) Prüfung in 366.093 Mikro- und Nanofluidik

Die Prüfungsatmosphäre ist sehr entspannt. Prüfung findet in Prof. Keplingers Büro statt. Er fragt einen anfangs, worüber man reden möchte, ich habe das Kapitel Mischer gewählt.

Die erste Frage war was das grundlegende Problem beim Mischen in der Mikrofluidik sei. Nachdem das erklärt war, ging es aber sehr schnell quer durch das ganze Stoffgebiet, eine Frage hat dann neue Fragen ergeben, usw.

Ich habe die Prüfung gleichzeitig mit einem anderen Kollegen gemacht, die Fragen wechselten da dynamisch zwischen uns beiden hin und her, es war quasi eine Gruppenprüfung.

Keplinger erklärt zwischendurch immer wieder etwas und prüft nicht allzu streng.